package com.response.imfviewer.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.response.imfviewer.R;

/**
 * IMFViewHolder.java - a viewHolder for holding the view of all messages.
 * @author  Bakr Kamil
 * @date 19-06-2020
 * @version 1.0
 */
public class IMFViewHolder extends RecyclerView.ViewHolder {

    protected TextView id, creationDate, interfaceId, destination, type;

    public IMFViewHolder(final View itemView, int layout) {
        super(itemView);
        // use layout with id 1
        if (layout == 1) {

            id = itemView.findViewById(R.id.id);
            creationDate = itemView.findViewById(R.id.creation_date);
            interfaceId = itemView.findViewById(R.id.interface_id);
            destination = itemView.findViewById(R.id.destination);
            type = itemView.findViewById(R.id.type);
        }
    }

}
