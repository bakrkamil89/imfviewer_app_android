package com.response.imfviewer.utils;

/**
 * Constants.java - a Constants for holding constants for all application.
 * @author  Bakr Kamil
 * @date 19-06-2020
 * @version 1.0
 */
public class Constants {
    // 10.0.2.2
    public static final String ALL_MESSAGES_URL="http://10.0.2.2:8080/";
    public static final String JSON_MESSAGES = "messages";
}
