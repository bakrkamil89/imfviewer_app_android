package com.response.imfviewer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import com.response.imfviewer.R;
import com.response.imfviewer.model.MessageModel;
import com.response.imfviewer.utils.IMFViewerSharedPreferences;

import java.util.List;

/**
 * IMFAdapter.java - a bridge between UI component and data source.
 * @author  Bakr Kamil
 * @date 19-06-2020
 * @version 1.0
 */
public class IMFAdapter extends RecyclerView.Adapter<IMFViewHolder> implements Filterable {

    List<MessageModel> messageList, filterList;
    private int layout;
    private Context context;
    private IMFViewHolder imfViewHolder;
    private FilterSearch filterSearch;
    private View view;
    private IMFViewerSharedPreferences imfViewerSharedPreferences;

    public IMFAdapter(Context context, List<MessageModel> messageList, int layout) {
        this.layout = layout;
        this.messageList = messageList;
        this.context = context;
        this.filterList = messageList;
        imfViewerSharedPreferences = new IMFViewerSharedPreferences(context);
    }

    @Override
    public IMFViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layout == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_message_item, null);
        }
        return new IMFViewHolder(view, layout);
    }

    @Override
    public void onBindViewHolder(IMFViewHolder holder, final int position) {

        int rowPos = holder.getAdapterPosition();
        imfViewHolder = holder;
        if (rowPos == 0) {

            setTableHeader();
        } else
            setTableContent(messageList.get(rowPos - 1));

        holder.interfaceId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(messageList.get(position).getInterfaceId());
            }
        });
    }

    @Override
    public int getItemCount() {

        int totalItems = imfViewerSharedPreferences.getTotalItems();
        if (messageList != null) if (totalItems == 1 || messageList.size() < totalItems) return messageList.size() + 1;

        return totalItems;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    private void setTableContent(MessageModel messageModel) {

        // Content Cells. Content appear here
        imfViewHolder.id.setBackgroundResource(R.drawable.table_content_cell_bg);
        imfViewHolder.creationDate.setBackgroundResource(R.drawable.table_content_cell_bg);
        imfViewHolder.interfaceId.setBackgroundResource(R.drawable.table_content_cell_bg);
        imfViewHolder.destination.setBackgroundResource(R.drawable.table_content_cell_bg);
        imfViewHolder.type.setBackgroundResource(R.drawable.table_content_cell_bg);

        String id = String.valueOf(messageModel.getId());
        imfViewHolder.id.setText(id);
        imfViewHolder.creationDate.setText(messageModel.getCreationDate());
        imfViewHolder.interfaceId.setText(messageModel.getInterfaceId());
        imfViewHolder.destination.setText(messageModel.getDestination());
        imfViewHolder.type.setText(messageModel.getType());
    }

    private void setTableHeader() {

        // Header Cells. Main Headings appear here
        imfViewHolder.id.setBackgroundResource(R.drawable.table_header_cell_bg);
        imfViewHolder.creationDate.setBackgroundResource(R.drawable.table_header_cell_bg);
        imfViewHolder.interfaceId.setBackgroundResource(R.drawable.table_header_cell_bg);
        imfViewHolder.destination.setBackgroundResource(R.drawable.table_header_cell_bg);
        imfViewHolder.type.setBackgroundResource(R.drawable.table_header_cell_bg);

        imfViewHolder.id.setText(context.getResources().getText(R.string._table_id));
        imfViewHolder.creationDate.setText(context.getResources().getText(R.string._table_creation_name));
        imfViewHolder.interfaceId.setText(context.getResources().getText(R.string._table_interface));
        imfViewHolder.destination.setText(context.getResources().getText(R.string._table_destination));
        imfViewHolder.type.setText(context.getResources().getText(R.string._table_type));
    }

    @Override
    public Filter getFilter() {

        if (filterSearch == null) {
            filterSearch = new FilterSearch(filterList, this);
        }
        return filterSearch;
    }
}