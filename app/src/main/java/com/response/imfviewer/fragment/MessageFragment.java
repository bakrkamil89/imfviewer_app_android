package com.response.imfviewer.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import androidx.appcompat.widget.SearchView;

import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.response.imfviewer.R;
import com.response.imfviewer.adapter.IMFAdapter;
import com.response.imfviewer.adapter.ItemClickListener;
import com.response.imfviewer.adapter.SimpleDividerItemDecoration;
import com.response.imfviewer.model.MessageModel;
import com.response.imfviewer.utils.FormValidation;
import com.response.imfviewer.utils.IMFViewerSharedPreferences;
import com.response.imfviewer.utils.MyDatePickerDialog;
import com.response.imfviewer.utils.UrlManager;

import java.util.Calendar;
import java.util.List;

/**
 * MessageFragment.java - a fragment for displaying all messages.
 * @author  Bakr Kamil
 * @date 19-06-2020
 * @version 1.0
 */
public class MessageFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private MessageViewModel messageViewModel;
    public List<MessageModel> messageModelList;
    private View rooView;
    private Dialog dialog;
    private IMFAdapter adapter;
    private RecyclerView recyclerView;
    private EditText interfaceText, startDate, endDate;
    private FormValidation formValidation;
    private MyDatePickerDialog myDatePickerDialog;
    private SearchView searchView;
    private FrameLayout frameLayoutSearchView;
    private IMFViewerSharedPreferences imfViewerSharedPreferences;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        formValidation = new FormValidation();
        messageViewModel = new ViewModelProvider(this).get(MessageViewModel.class);
        rooView = inflater.inflate(R.layout.fragment_message, container, false);
        imfViewerSharedPreferences = new IMFViewerSharedPreferences(getContext());
        FloatingActionButton floatingActionButton = rooView.findViewById(R.id.fab);
        setDialogView();
        setupSpinner();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                openDialog();
            }
        });
        return rooView;
    }

    /**
     * Initiate dialog layout in order to fetch data on demand.
     */
    public void setDialogView() {
        dialog = new Dialog(getContext());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_dialog);
    }

    public void openDialog() {

        // initiate fields
        initiateInterfaceIdField();
        InitiatePickStartDate();
        InitiatePickEndDate();

        // initiate buttons
        fetchByItemButton();
        resetFormButton();
        fetchAllButton();

        dialog.show();
        closeDialog();
    }

    // initiate close dialog button
    private void closeDialog() {

        ImageView closeDialog = dialog.findViewById(R.id.close_dialog);
        closeDialog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                emptyFields();
                dialog.dismiss();
            }
        });
    }

    // fetch all messages from DB.
    private void fetchAllRemoteMessages() {

        messageViewModel.getAllMessages().observe(getViewLifecycleOwner(), new Observer<List<MessageModel>>() {

            @Override
            public void onChanged(List<MessageModel> data) {
                messageModelList = data;
                setAdapter();
            }
        });
    }

    public void setupSpinner() {

        Spinner spinner = dialog.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        String[] spinnerElements = getContext().getResources().getStringArray(R.array.spinner_elements);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_item, spinnerElements);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

        String selectedValue = adapterView.getItemAtPosition(position).toString();
        if (position > 0) {
            if (position == 5) imfViewerSharedPreferences.setItem(1);
            else if (position == 6) imfViewerSharedPreferences.setItem(0);
            else {
                imfViewerSharedPreferences.setItem(Integer.parseInt(selectedValue));
            }
        } else {
            imfViewerSharedPreferences.setItem(imfViewerSharedPreferences.getTotalItems());
        }
        checkAdapter();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        imfViewerSharedPreferences.setItem(1);
    }

    // Notify adapter about changes
    private void checkAdapter() {
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    private void fetchByItemButton() {

        Button search = dialog.findViewById(R.id.search_per_item);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UrlManager.isOnline(getActivity())) {
                    initiateInterfaceIdField();
                    if (formValidation.validateDate()) {
                        getMessagesByDates();
                        setSearchView();
                        emptyFields();
                    } else if (formValidation.ValidateInterface()) {
                        getMessagesByInterfaceId();
                        setSearchView();
                        emptyFields();
                    } else if (formValidation.ValidateDateAndInterface()) {
                        getMessagesByInterfaceIdAndDates();
                        setSearchView();
                        emptyFields();

                    } else if (formValidation.checkFormFields()) {
                        Toast.makeText(getContext(), getContext().getResources().
                                getString(R.string.empty_form), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), getContext().getResources().
                                getString(R.string.wrong_combination), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), getContext().getResources().
                            getString(R.string.is_online), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void fetchAllButton() {

        Button fetchAllData = dialog.findViewById(R.id.fetch_all);
        fetchAllData.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (UrlManager.isOnline(getActivity())) {
                    imfViewerSharedPreferences.setItem(1);
                fetchAllRemoteMessages();
                setSearchView();
                dialog.dismiss();
            } else {
                    Toast.makeText(getContext(), getContext().getResources().
                            getString(R.string.is_online), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // clear all field in the form
    private void resetFormButton() {

        Button resetForm = dialog.findViewById(R.id.reset_form);
        resetForm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resetAll();
            }
        });
    }

    private void resetAll() {

        imfViewerSharedPreferences.setItem(imfViewerSharedPreferences.getTotalItems());
        emptyFields();
        setAdapter();
        setupSpinner();
        //if (frameLayoutSearchView != null) frameLayoutSearchView.setVisibility(View.INVISIBLE);
        //setHorizontalScrollViewLayout(0);
    }

    private void emptyFields() {
        interfaceText.setText(null);
        startDate.setText(null);
        endDate.setText(null);
        formValidation.setInterfaceId(null);
        formValidation.setEndDateString(null);
        formValidation.setStartDateString(null);
        imfViewerSharedPreferences.setItem(1);
    }

    public void initiateInterfaceIdField() {

        interfaceText = dialog.findViewById(R.id.search_item_text);
        formValidation.setInterfaceId(interfaceText.getText().toString().trim());
    }

    private void InitiatePickStartDate() {

        startDate = dialog.findViewById(R.id.start_date_text);
        startDate.setEnabled(false);
        final ImageView startDatePicker = dialog.findViewById(R.id.start_date_picker);
        startDatePicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                setMyDatePickerDialog();
                myDatePickerDialog.showDatePicker(new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        int selectedMonth = month + 1;
                        formValidation.setStartDateString(dayOfMonth + "-" + selectedMonth + "-" + year);
                        startDate.setText(formValidation.getStartDateString());
                    }
                }, Calendar.getInstance());
            }
        });
    }

    private void InitiatePickEndDate() {

        endDate = dialog.findViewById(R.id.end_date_text);
        endDate.setEnabled(false);
        final ImageView endDatePicker = dialog.findViewById(R.id.end_date_picker);
        endDatePicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                setMyDatePickerDialog();
                myDatePickerDialog.showDatePicker(new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        int selectedMonth = month + 1;
                        formValidation.setEndDateString(dayOfMonth + "-" + selectedMonth + "-" + year);
                        endDate.setText(formValidation.getEndDateString());
                    }
                }, Calendar.getInstance());
            }
        });
    }

    private void setMyDatePickerDialog() {

        myDatePickerDialog =  new MyDatePickerDialog(getContext());
        dialog.setTitle(getActivity().getResources().getString(R.string.set_date));
    }

    private void setAdapter() {

        recyclerView = getRecyclerReference();
        adapter = new IMFAdapter(getContext(), messageModelList, 1);
        // set fixed size
        recyclerView.setHasFixedSize(true);

        // set adapter
        recyclerView.setAdapter(adapter);

        // lazy loading
        recyclerView.clearOnScrollListeners();
    }

    private RecyclerView getRecyclerReference() {

        if (rooView != null) {

            recyclerView = rooView.findViewById(R.id.recyclerViewMovieList);
            return setRecyclerLayout(recyclerView);
        } else return null;
    }

    /**
     * Set RecyclerView layout.
     * @return A RecyclerView data type.
     */
    private RecyclerView setRecyclerLayout(RecyclerView recyclerView) {

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return recyclerView;
    }

    // fetch messages by interface Id and (start & end dates)
    private void getMessagesByInterfaceIdAndDates() {

        initiateInterfaceIdField();
        messageViewModel.getByInterfaceIdAndDate(formValidation.getInterfaceId(),
                formValidation.getStartDateString(), formValidation.getEndDateString())
                .observe(getViewLifecycleOwner(), new Observer<List<MessageModel>>() {
                    @Override
                    public void onChanged(List<MessageModel> data) {

                        messageModelList = data;
                        setAdapter();
                        dialog.dismiss();
                    }
                });
    }

    // fetch messages by interface Id
    private void getMessagesByInterfaceId() {

        messageViewModel.getByInterfaceId(formValidation.getInterfaceId())
                .observe(getViewLifecycleOwner(), new Observer<List<MessageModel>>() {
                    @Override
                    public void onChanged(List<MessageModel> data) {
                        messageModelList = data;
                        setAdapter();
                        dialog.dismiss();
                    }
                });
    }

    // fetch messages by start & end date
    private void getMessagesByDates() {

        messageViewModel.getByDates(formValidation.getStartDateString(), formValidation.getEndDateString())
                .observe(getViewLifecycleOwner(), new Observer<List<MessageModel>>() {
                    @Override
                    public void onChanged(List<MessageModel> data) {
                        messageModelList = data;
                        imfViewerSharedPreferences.setItem(1);
                        setAdapter();
                        dialog.dismiss();
                    }
                });
    }

    private void setSearchView() {

        setHorizontalScrollViewLayout(100);
        frameLayoutSearchView = rooView.findViewById(R.id.frame_id);
        initiateSearchView();

        // display frameLayout for the search view
        frameLayoutSearchView.setVisibility(View.VISIBLE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    // set margin for the table so that the search field appears when messages are fetched
    private void setHorizontalScrollViewLayout(int topMargin) {

        HorizontalScrollView horizontalScrollView = rooView.findViewById(R.id.horizontalScrollView);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) horizontalScrollView.getLayoutParams();
        params.setMargins(0, topMargin, 0, 0);
        horizontalScrollView.setLayoutParams(params);
    }

    private void initiateSearchView() {

        searchView = rooView.findViewById(R.id.search_view);
        searchView.setIconified(false);
        searchView.clearFocus();
        SearchView.SearchAutoComplete searchAutoComplete = rooView.findViewById(R.id.search_src_text);
        searchAutoComplete.setTextColor(ContextCompat.getColor(getContext(), R.color.global_color_green_primary));
        searchView.setQueryHint(getContext().getResources().getString(R.string.search_hint));
    }
}