package com.response.imfviewer.adapter;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(Object object);
    void onItemLongClick(int position, View v);}