package com.response.imfviewer.model;

/**
 * Message.java - a Message for holding constants messages.
 * @author  Bakr Kamil
 * @date 19-06-2020
 * @version 1.0
 */
public enum Message {
    id,
    dateCreated,
    interfaceId,
    destination,
    type
}