package com.response.imfviewer.utils;

public class FormValidation {

    private String startDateString;
    private String endDateString;
    private String interfaceId;

    public String getStartDateString() {
        return startDateString;
    }

    public void setStartDateString(String startDateString) {
        this.startDateString = startDateString;
    }

    public String getEndDateString() {
        return endDateString;
    }

    public void setEndDateString(String endDateString) {
        this.endDateString = endDateString;
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public boolean ValidateInterface() {

        return (getInterfaceId() != null && !getInterfaceId().isEmpty()) &&
                (getStartDateString() == null && getEndDateString() == null);

    }

    public boolean validateDate() {
        return  getStartDateString() != null && getEndDateString() != null && getInterfaceId() == null;
    }

    public boolean ValidateDateAndInterface() {
        System.out.println(getEndDateString());
        System.out.println(getStartDateString());
        System.out.println(getInterfaceId());
        return  getStartDateString() != null && getEndDateString() != null && getInterfaceId() != null;
    }

    public boolean checkFormFields() {
        return (getInterfaceId() == null || getInterfaceId().equals("") &&
                ((getStartDateString() == null || getStartDateString().equals("")) &&
                        (getEndDateString() == null || getEndDateString().equals(""))));
    }

}
