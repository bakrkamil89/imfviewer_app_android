package com.response.imfviewer.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageModel {

    private int id;
    private String creationDate;
    private String interfaceId;
    private String destination;
    private String Type;
}
