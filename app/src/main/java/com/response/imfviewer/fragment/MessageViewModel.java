package com.response.imfviewer.fragment;

import android.os.AsyncTask;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.response.imfviewer.model.MessageModel;
import com.response.imfviewer.utils.Constants;
import com.response.imfviewer.utils.JsonParser;
import com.response.imfviewer.utils.UrlManager;

import java.util.ArrayList;
import java.util.List;

public class MessageViewModel extends ViewModel {

    public static MutableLiveData<List<MessageModel>> mutableLiveData = new MutableLiveData<>();
    private String url;

    public MutableLiveData<List<MessageModel>> getAllMessages() {

        this.url = Constants.ALL_MESSAGES_URL + "all";
        AsyncMessages asyncMessages = new AsyncMessages();
        asyncMessages.execute();

        return mutableLiveData;
    }

    public MutableLiveData<List<MessageModel>> getByInterfaceId(String interfaceId) {


        this.url = Constants.ALL_MESSAGES_URL + "interface?interfaceId=" + interfaceId;
        AsyncMessages asyncMessages = new AsyncMessages();
        asyncMessages.execute();

        return mutableLiveData;
    }

    public MutableLiveData<List<MessageModel>> getByDates(String startDate, String endDate) {


        this.url = Constants.ALL_MESSAGES_URL + "dates?startDate=" + startDate +
                "&endDate=" + endDate;

        AsyncMessages asyncMessages = new AsyncMessages();
        asyncMessages.execute();

        return mutableLiveData;
    }

    public MutableLiveData<List<MessageModel>> getByInterfaceIdAndDate(String interfaceId, String startDate, String endDate) {


        this.url = Constants.ALL_MESSAGES_URL + "interface_dates?interfaceId=" + interfaceId +
                "&startDate=" + startDate + "&endDate=" + endDate;

        AsyncMessages asyncMessages = new AsyncMessages();
        asyncMessages.execute();

        return mutableLiveData;
    }

    private class AsyncMessages extends AsyncTask<Void, Void, Void> {

        List<MessageModel> messageModelList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String data = UrlManager.getRemoteMessages(url);

            if (data!= null) messageModelList = JsonParser.parseMessagesJson(data);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mutableLiveData.setValue(messageModelList);
        }
    }
}