package com.response.imfviewer.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * UrlManager.java - a UrlManager for make online connection and reading data from stream.
 * @author  Bakr Kamil
 * @date 19-06-2020
 * @version 1.0
 */
public class UrlManager {

    static HttpURLConnection connection;
    static BufferedReader reader;
    static URL url;
    static InputStream stream;
    static StringBuilder stringBuilder;

    public static String getRemoteMessages(String url) {

        String line = null;
        try {
            makeConnection(url);

            while ((line = reader.readLine()) != null) {

                stringBuilder.append(line + "\n");
            }
            line = stringBuilder.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            if (connection != null) {

                connection.disconnect();
            }
            try {

                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        return line;
    }

    private static void makeConnection(String urlPassed) throws IOException {

        url = new URL(urlPassed);
        connection = (HttpURLConnection) url.openConnection();
        connection.connect();

        stream = connection.getInputStream();
        stringBuilder = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(stream));
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
