package com.response.imfviewer.utils;

import android.util.Log;

import com.response.imfviewer.model.Message;
import com.response.imfviewer.model.MessageModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * JsonParser.java - a JsonParser for parsing json to objects for all application.
 * @author  Bakr Kamil
 * @date 19-06-2020
 * @version 1.0
 */
public class JsonParser {

    static JSONArray jsonArray;

    public static List<MessageModel> parseMessagesJson(String messagesString) {

        System.out.println(messagesString);

        List<MessageModel> messageModelList = new ArrayList<>();
        try {

            jsonArray = new JSONArray(messagesString);
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MessageModel messageModel = new MessageModel();
                messageModel.setId(Integer.parseInt(jsonObject.getString(Message.id.toString())));
                try {
                    System.out.println(convertDate(jsonObject.getString(Message.dateCreated.toString())));
                    messageModel.setCreationDate(convertDate(jsonObject.getString(Message.dateCreated.toString())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                messageModel.setInterfaceId(jsonObject.getString(Message.interfaceId.toString()));
                messageModel.setDestination(jsonObject.getString(Message.destination.toString()));
                messageModel.setType(jsonObject.getString(Message.type.toString()));
                messageModelList.add(messageModel);
            }
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        return messageModelList;
    }

    // temp method
    private static String convertDate(String startDateStr) throws ParseException {

        // TODO: 6/30/20
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD hh:mm");
        return formatter.format(date);
    }
}
