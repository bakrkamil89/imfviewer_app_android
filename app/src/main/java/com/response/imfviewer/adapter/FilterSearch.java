package com.response.imfviewer.adapter;

import com.response.imfviewer.model.MessageModel;

import java.util.ArrayList;
import java.util.List;

public class FilterSearch extends android.widget.Filter {

    private List<MessageModel> messageModelList;

    private IMFAdapter imfAdapter;

    public FilterSearch(List<MessageModel> messageModelList, IMFAdapter imfAdapter) {

        this.messageModelList = messageModelList;
        this.imfAdapter = imfAdapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {

        FilterResults results = new FilterResults();
        if (charSequence != null && charSequence.length() > 0) {

            charSequence = charSequence.toString().toUpperCase();
            List<MessageModel> messagesArrayList = new ArrayList<>();
            for (int i = 0; i < messageModelList.size(); i++) {

                if (messageModelList.get(i).getInterfaceId().toUpperCase().contains(charSequence) ||
                        messageModelList.get(i).getDestination().toUpperCase().contains(charSequence) ||
                        messageModelList.get(i).getType().toUpperCase().contains(charSequence) ||
                        messageModelList.get(i).getCreationDate().toUpperCase().contains(charSequence))
                {
                    messagesArrayList.add(messageModelList.get(i));
                }
            }
            results.count = messagesArrayList.size();
            results.values = messagesArrayList;
        } else {
            results.count = messageModelList.size();
            results.values = messageModelList;
        }
        return results;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        imfAdapter.messageList = (List<MessageModel>) filterResults.values;
        imfAdapter.notifyDataSetChanged();
    }

}
