package com.response.imfviewer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class IMFViewerSharedPreferences {

    public static final String PREF_NAME = "IMFViewer";

    private static final String KEY_ITEM = "item";

    private SharedPreferences sharedPreferences;

    private Context context;

    public int PRIVATE_MODE = 0;

    private Editor editor;

    public IMFViewerSharedPreferences(Context context) {

        this.context = context;

        this.sharedPreferences = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);

        this.editor = sharedPreferences.edit();
    }

    public void setItem(int item) {

        editor.putInt(KEY_ITEM, item).commit();
    }

    public int getTotalItems() {

        return sharedPreferences.getInt(KEY_ITEM,1);
    }
}
